# base64

## Encode

To encode a file:

   ```bash
   base64 <file>
   ``` 
   
Then add 

    ```
    background: url(data:image/svg+xml;base64,<string>) no-repeat;
    ```

## Decode

To decode a base64 string
without `base64,` at beginning and `)` at and (as used in css)

    ```bash
    echo "<string>" | base64 --decode >> file
    ```
